﻿using liblib.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace liblib.Infrastructure.Repositories
{
    public class BookRepository : IBookRepository
    {
        private readonly LiblibContext db;
        public BookRepository(LiblibContext db)
        {
            this.db = db;
        }
        public void Add(Book book)
        {
            db.Books.Add(book);
            db.SaveChanges();
        }

        public IEnumerable<Book> Books()
        {
            return this.db.Books;
        }

        public void Delete(string id)
        {
            var book = db.Books.Find(id);
            db.Books.Remove(book);
            db.SaveChanges();
        }

        public void Edit(Book book)
        {
            db.Entry(book).State = EntityState.Modified;
            db.SaveChanges();
        }

        public Book Find(string id)
        {
            Book book = db.Books.Find(id);
            return book;
        }
    }
}
