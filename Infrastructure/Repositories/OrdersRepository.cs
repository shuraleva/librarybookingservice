﻿using liblib.Infrastructure.Services;
using liblib.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace liblib.Infrastructure.Repositories
{
    public class OrdersRepository : IOrdersRepository
    {
        private readonly LiblibContext db;
        public OrdersRepository(LiblibContext db)
        {
            this.db = db;
        }
        public void Add(Order order)
        {
            db.Orders.Add(order);
            db.SaveChanges();
        }

        public bool BookIsGiven(string id)
        {
            if (db.Orders.Find(id).DateBookIsGiven.Equals(null))
            { 
                return false; 
            }
            else
            {
                return true;
            }
        }

        public bool BookIsReturned(string id)
        {
            if (db.Orders.Find(id).DateBookReturned.Equals(null))
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public void Delete(string id)
        {
            var order = db.Orders.Find(id);
            db.Orders.Remove(order);
            db.SaveChanges();
        }

        public void Edit(Order order)
        {
            db.Entry(order).State = EntityState.Modified;
            db.SaveChanges();
        }

        public Order Find(string id)
        {
            var order = db.Orders.Find(id);
            return order;
        }
        public IEnumerable<Order> Orders()
        {
            return this.db.Orders.Include(u => u.User)
                .Include(b => b.Book);
        }
    }
}
