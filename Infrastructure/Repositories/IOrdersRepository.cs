﻿using liblib.Models;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace liblib.Infrastructure.Repositories
{
    public interface IOrdersRepository
    {
        void Add(Order order);
        void Delete(string id);
        void Edit(Order order);
        Order Find(string id);


        IEnumerable<Order> Orders();
        //void DateTime SetTimeToReturn(DateTime dt);
        //void DateTime SetTimeBookWasGiven(DateTime dt);
        //void DateTime SetTimeBookWasReturned(DateTime dt);

        //void SetTimeToReturn(DateTime dt);
        //void SetTimeBookWasGiven(DateTime dt);
        //void SetTimeBookWasReturned(DateTime dt);
        //void SetDateBookingCreated(DateTime dt);
        //Boolean BookIsGiven(string id);
        //Boolean BookIsReturned(string id);
        
    }
}
