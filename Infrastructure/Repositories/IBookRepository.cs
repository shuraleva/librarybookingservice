﻿using liblib.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace liblib.Infrastructure.Repositories
{
    public interface IBookRepository
    {
        void Add(Book book);
        void Delete(string id);
        void Edit(Book book);
        Book Find(string id);
        IEnumerable<Book> Books();
    }
}
