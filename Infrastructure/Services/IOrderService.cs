﻿using liblib.Models;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace liblib.Infrastructure.Services
{
    public interface IOrderService
    {
        Order Find(string id);
        IEnumerable<Order> Orders();
        void Add(Order order);
        void Delete(string id);
        void Edit(Order order);
        void CancelOrder(string id);
        void SetDateBookingCreated(string id);
        void SetTimeBookWasGiven(string id);
        void SetTimeToReturn(string id);
        void SetTimeBookWasReturned(string id);
        Boolean BookIsGiven(string id);
        Boolean BookIsReturned(string id);
    }
}
