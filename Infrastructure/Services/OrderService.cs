﻿using liblib.Infrastructure.Repositories;
using liblib.Models;
using liblib.Infrastructure.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace liblib.Infrastructure.Services
{
    public class OrderService : IOrderService
    {
        private readonly IOrdersRepository ordersRepository;
        private readonly IUserService userService;

        public OrderService(IOrdersRepository ordersRepository, IUserService userService)
        {
            this.ordersRepository = ordersRepository;
            this.userService = userService;
        }
        public void Add(Order order)
        {
            order.Id = Guid.NewGuid().ToString();
            order.DateBookingCreated = DateTime.Now;
            this.ordersRepository.Add(order);
        }

        public bool BookIsGiven(string id)
        {
            throw new NotImplementedException();
        }

        public bool BookIsReturned(string id)
        {
            throw new NotImplementedException();
        }

        public void CancelOrder(string id)
        {
            var order = this.ordersRepository.Find(id);
            order.DateBookReturned = DateTime.Now;
            Edit(order);
        }

        public void Delete(string id)
        {
            this.ordersRepository.Delete(id);
        }

        public void Edit(Order order)
        {
            this.ordersRepository.Edit(order);
        }

        public Order Find(string id)
        {
            return this.ordersRepository.Find(id);
        }

        public IEnumerable<Order> Orders()
        {
            return this.ordersRepository.Orders();
        }

        public void SetDateBookingCreated(string id)
        {
            var order = this.ordersRepository.Find(id);
            
            Edit(order);
        }

        public void SetTimeBookWasGiven(string id)
        {
            var order = this.ordersRepository.Find(id);
            order.DateBookIsGiven = DateTime.Now;
            SetTimeToReturn(order.Id);
            Edit(order);
        }

        public void SetTimeBookWasReturned(string id)
        {
            var order = this.ordersRepository.Find(id);
            order.DateBookReturned = DateTime.Now;
            this.ordersRepository.Edit(order);
        }

        public void SetTimeToReturn(string id)
        {
            var order = this.ordersRepository.Find(id);
            order.DateBookToReturn = order.DateBookIsGiven.AddDays(14);
            this.ordersRepository.Edit(order);
        }
    }
}