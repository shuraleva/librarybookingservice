﻿using liblib.Infrastructure.Repositories;
using liblib.Infrastructure.Services;
using liblib.Models;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using liblib.Areas.Identity;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;

namespace liblib.Infrastructure.Services
{
    public class BookService : IBookService
    {
        private readonly IBookRepository bookRepository;
        private readonly IUserService userService;
        private readonly IOrderService orderService;

        public BookService(IBookRepository bookRepository, IUserService userService, IOrderService orderService)
        {
            this.bookRepository = bookRepository;
            this.userService = userService;
            this.orderService = orderService;
        }
        public void Add(Book book)
        {
            book.Id = Guid.NewGuid().ToString();
            this.bookRepository.Add(book);
        }

        public IEnumerable<Book> Books()
        {
            return this.bookRepository.Books();
        }

        public void Delete(string id)
        {
            this.bookRepository.Delete(id);
        }

        public void Edit(Book book)
        {
            this.bookRepository.Edit(book);
        }

        public Book Find(string id)
        {
            return this.bookRepository.Find(id);
        }

        public string GetBookName(Book book)
        {
            var book1 = bookRepository.Find(book.Id);
            var bookName = book1.Name;
            return bookName;
        }

        //в разработке
        public void Reserve(string id)
        {
            if (id != null)
            {
                var book = this.bookRepository.Find(id);
                var userId = this.userService.GetUserId();
                var user = this.userService.GetUser(userId);
                if (user != null && book.Quantity > 0)
                {
                    Order order = new()
                    {
                        Book = book,
                        User = user
                        
                    };
                    orderService.Add(order);
                    
                    if (order != null)
                    {
                        book.Quantity--;
                        bookRepository.Edit(book);
                    }
                }
            }
                  }
    }
}
    
    
