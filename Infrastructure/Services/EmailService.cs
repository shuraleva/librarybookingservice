﻿using MimeKit;
using MailKit.Net.Smtp;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.UI.Services;
//using System.Net.Mail;

namespace liblib.Infrastructure.Services
{
    public class EmailService : IEmailSender
    {
        public async Task SendEmailAsync(string email, string subject, string message)
        {
            var emailMessage = new MimeMessage();

            emailMessage.From.Add(new MailboxAddress("Администрация сайта LibBookService", "t.pokasova@yandex.ru"));
            emailMessage.To.Add(new MailboxAddress("", email));
            emailMessage.Subject = subject;
            emailMessage.Body = new TextPart(MimeKit.Text.TextFormat.Html)
            {
                Text = message
            };

            using var client = new SmtpClient();
            await client.ConnectAsync("smtp.yandex.ru", 587, false);
            await client.AuthenticateAsync("t.pokasova@yandex.ru", "ahslyxpeaiunbhdi");
            await client.SendAsync(emailMessage);

            await client.DisconnectAsync(true);
        }
    }
}