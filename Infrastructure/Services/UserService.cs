﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace liblib.Infrastructure.Services
{
    public class UserService : IUserService
    {
        private readonly IHttpContextAccessor _httpContext;
        private readonly UserManager<IdentityUser> _userManager;
        public UserService(IHttpContextAccessor httpContext, UserManager<IdentityUser> userManager)
        {
            _httpContext = httpContext;
            _userManager = userManager;
        }

        public IdentityUser GetUser(string id)
        {
            return _userManager.FindByIdAsync(id).Result;  
        }

        public string GetUserEmail()
        {
            return _httpContext.HttpContext.User?.FindFirstValue(ClaimTypes.Email);
        }

        public string GetUserId()
        {
            return _httpContext.HttpContext.User?.FindFirstValue(ClaimTypes.NameIdentifier);
            //return _userManager.GetUserId(ClaimsPrincipal.Current);
        }
    }
}
