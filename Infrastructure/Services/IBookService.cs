﻿using liblib.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace liblib.Infrastructure.Services
{
    public interface IBookService
    {
        Book Find(string id);
        IEnumerable<Book> Books();
        void Add(Book book);
        void Delete(string id);
        void Edit(Book book);
        void Reserve(string id);
        string GetBookName(Book book);
    }
}
