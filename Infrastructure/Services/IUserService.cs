﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace liblib.Infrastructure.Services
{
    public interface IUserService
    {
        string GetUserId();
        string GetUserEmail();
        public IdentityUser GetUser(string id);
    }
}
