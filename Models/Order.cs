﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace liblib.Models
{
    //в разработке
    public class Order
    {
        public string Id { get; set; }
        public IdentityUser User { get; set; }
        public Book Book { get; set; }
        public DateTime DateBookingCreated { get; set; }
        public DateTime DateBookIsGiven { get; set; }
        public DateTime DateBookToReturn { get; set; }
        public DateTime DateBookReturned { get; set; }
        //IEnumerable<Order> Orders;

    }
}
