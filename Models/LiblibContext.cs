﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using liblib.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI;
using System.Configuration;


#nullable disable

namespace liblib
{
 //   public partial class LiblibContext :
 //   IdentityDbContext<ApplicationUser>
 //   {
 //}
    public partial class LiblibContext : IdentityDbContext 
    {

        public DbSet<Book> Books { get; set; }
        public DbSet<Order> Orders { get; set; }

        public LiblibContext(DbContextOptions<LiblibContext> options)
            : base(options)
        {
            this.ChangeTracker.LazyLoadingEnabled = false;
        }
    

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseNpgsql("Host=localhost;Port=5432;Database=BookingApp1;Username=postgres;Password=2248");

            }
        }

        //protected override void OnModelCreating(ModelBuilder modelBuilder)
        //{

        //    modelBuilder.HasAnnotation("Relational:Collation", "Russian_Russia.1251");

        //    OnModelCreatingPartial(modelBuilder);
        //}

        //partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
