﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace liblib.Models
{
    public class Book
    {

        public string Id { get; set; }
        [Display(Name = "Название")]
        public string Name { get; set; }
        [Display(Name = "Издательство")]
        public string Publisher { get; set; }
        [Display(Name = "Автор")]
        public string Author { get; set; }
        [Display(Name = "Количество")]
        public int Quantity { get; set; }
        //public Book (string id, string name, string publisher, string author, int quantity) {
        //    Id = id;
        //    Name = name;
        //    Publisher = publisher;
        //    Author = author;
        //    Quantity = quantity;
        //}
        //public Book() { }
    }
}
       
