﻿using liblib.Models;
using liblib;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.AspNetCore.Mvc;
using System.Text.Encodings.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using liblib.Infrastructure.Services;

namespace liblib.Controllers
{
    [Authorize(Roles ="librarian")]
    public class BookController : Controller
    {

        private readonly IBookService bookService;
        public BookController(IBookService bookService)
        {
            this.bookService = bookService;
        }

        [HttpGet]
        public IActionResult Index()
        {
            var books = bookService.Books();
            
            return View(books);
        }

        [HttpPost]
        public IActionResult Index(string searchString)
        {
            var books = this.bookService.Books();

            if (!String.IsNullOrEmpty(searchString))
            {
                string searchStringLow = searchString.ToLower();
                books = books.Where(s =>
                s.Name.ToLower().Contains(searchStringLow)
                || s.Author.ToLower().Contains(searchStringLow));
                return View(books);
            }
            return View("Index");
        }

        [HttpGet]
        public IActionResult Add()
        {
            return View("Add");
        }
    
        [HttpPost]
        public IActionResult Add(Book book)
        {
            if (book != null)
            {
                this.bookService.Add(book);
            }
            return RedirectToAction("Index");
        }

        public IActionResult Delete(String id)
        {
            this.bookService.Delete(id);
            return RedirectToAction("Index");
        }

        //public IActionResult EditPage()
        //{ return View("EditPage"); }

        [HttpGet]
        public ActionResult EditBook(String id)
        {

            Book book = this.bookService.Find(id);
            return View(book);

        }

        [HttpPost]
        public ActionResult EditBook(Book book)
        {
            this.bookService.Edit(book);
            return RedirectToAction("Index");
        }
    }
}


