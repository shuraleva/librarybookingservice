﻿using liblib.Infrastructure.Services;
using liblib.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace liblib.Controllers
{
    public class HomeController : Controller
    {
        private readonly IBookService bookService;
        private readonly IUserService userService;
        public HomeController(IBookService bookService, IUserService userService)
        {
            this.userService = userService;
            this.bookService = bookService;
        }


        public IActionResult Index()
        {
            var books = this.bookService.Books();
            return View(books);
        }

        //в разработке
        [Authorize]
        public IActionResult Reserve(string id)
        {
            if (!String.IsNullOrWhiteSpace(id))
            {
                this.bookService.Reserve(id);
            }
            return RedirectToAction("Index");
        }
        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
