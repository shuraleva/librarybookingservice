﻿using liblib.Infrastructure.Services;
using liblib.Models;
using liblib.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace liblib.Controllers
{
    [Authorize(Roles = "librarian")]
    public class OrdersController : Controller
    {
        private readonly IBookService bookService;
        private readonly IUserService userService;
        private readonly IOrderService orderService;
        private readonly UserManager<IdentityUser> userManager;
        private readonly LiblibContext db;
        public OrdersController(IBookService bookService, IUserService userService, IOrderService orderService, UserManager<IdentityUser> userManager, LiblibContext db)
        {
            this.userService = userService;
            this.bookService = bookService;
            this.orderService = orderService;
            this.userManager = userManager;
            this.db = db;
        }
        public IActionResult Index()
        {
            var orders = orderService.Orders();

            return View(orders);
        }
        public IActionResult GiveOut(string id)
        {
            if (id != null)
            {
                this.orderService.SetTimeBookWasGiven(id);
            }
            return RedirectToAction("Index");
        }
        public IActionResult TakeBack(string id)
        {
            if (id != null)
            {
                var orders = orderService.Orders();
                this.orderService.CancelOrder(id);
                var order = orders.Where(o => o.Id==id).Single();
                var book = order.Book;
                book.Quantity++;
                bookService.Edit(book);

            }
            return RedirectToAction("Index");
        }
        public async Task<IActionResult> SendMessage(string id)
        {
            var orders = orderService.Orders();
            var order = orders.Where(i => i.Id==id).Single();
            EmailService emailService = new EmailService();
            await emailService.SendEmailAsync(order.User.Email, "Напоминание!", "В соответствии с правилами библиотеки необходимо вернуть книгу" + order.DateBookToReturn);
            return RedirectToAction("Index");
        }
    }
}
