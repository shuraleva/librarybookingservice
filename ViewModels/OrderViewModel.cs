﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace liblib.ViewModels
{
    public class OrderViewModel
    {
        public string Email { get; set; }
        public string Name { get; set; }
        public DateTime DateBookingCreated { get; set; }
        public DateTime DateBookIsGiven { get; set; }
        public DateTime DateBookToReturn { get; set; }
        public DateTime DateBookReturned { get; set; }
        public string Id { get; set; }

    }
}
